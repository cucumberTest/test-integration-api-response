package response;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestIntegrationResponse {

    @RequestMapping(value = "/response",method = RequestMethod.GET)
    @ResponseBody
    public static String response() {
        final String retrurnValue = "OK";
        return retrurnValue;
    }
}
